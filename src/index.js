import ReactDOM from "react-dom"
import { App } from "./components/App"
import * as serviceWorker from "./serviceWorker"
import React from "react"

ReactDOM.render(
    <App />,
    document.getElementById('root')
)

serviceWorker.unregister()