import { observable, configure, action } from "mobx"

configure({enforceActions: "always"})

export const ui = observable({
    loading: false,
    showDrawer: false,
    pageTitle: null,
    menu: [
        {
            title: 'Текущее меню',
            icon: 'Restaurant',
            route: 'current-menu'
        },
        {
            title: 'Список покупок',
            icon: 'LocalGroceryStore',
            route: 'shopping-list'
        },
        {
            title: 'Готовые меню',
            icon: 'ListAlt',
            route: 'menus'
        },
        {
            title: 'Мои меню',
            icon: 'ListAlt',
            route: 'my-menus'
        },
        {
            title: 'Все рецепты',
            icon: 'ListAlt',
            route: 'recipes'
        },
        {
            title: 'Избранные рецепты',
            icon: 'FavoriteBorder',
            route: 'favorite-recipes'
        },
        {
            title: 'Настройки',
            icon: 'Settings',
            route: 'settings'
        },
        {
            title: 'Обратная связь',
            icon: 'HeadsetMic',
            route: 'support'
        },
        {
            title: 'Пригласить друга',
            icon: 'PersonAdd',
            route: 'referral'
        },
    ],

    setLoading() {
        this.loading = true
    },

    unsetLoading() {
        this.loading = false
    },

    toggleDrawer() {
        this.showDrawer = !this.showDrawer
    },

    setPageTitle(title) {
        this.pageTitle = title
    },

    getPageTitle(route = null) {

        let title = this.pageTitle

        if(!title) {
            const currentMenuItem = this.menu.find(menuItem => menuItem.route === route)
            title = currentMenuItem ? currentMenuItem.title : null
        }

        if(title) {
            document.title = title
        }

        return title
    }
}, {
    setLoading:action('Set Loading'),
    unsetLoading:action('Unset Loading'),
    setPageTitle:action('Set Page Title'),
    toggleDrawer: action('Toggle Drawer')
}, {
    name: 'UI store'
})