import { observable, configure, action } from "mobx"
import { apiGetMenus } from "../services/menus"
import { ui } from "./ui"

configure({enforceActions: "always"})

export const menus = observable({
    menusFetched: false,
    menus: [],

    getMenus() {
        ui.setLoading()
        apiGetMenus().then(this.getMenusSuccess, this.getMenusError).then(this.getMenusFinished)
    },

    getMenusSuccess(menus) {
        this.menus = menus
        console.log(menus)
    },

    getMenusError(error) {
        console.log(error)
    },

    getMenusFinished() {
        this.menusFetched = true
        ui.unsetLoading()
    }
}, {
    getMenus:action('Get Menus'),
    getMenusSuccess:action.bound,
    getMenusFinished:action.bound,
}, {
    name: 'Menus store'
})