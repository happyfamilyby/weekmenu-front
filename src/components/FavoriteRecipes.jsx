import React from 'react'

import {
    Typography,
} from '@material-ui/core'

import {observer} from "mobx-react"

export const FavoriteRecipes = observer(({ uiState }) => {

    return(
        <Typography variant="h2">Favorite Recipes</Typography>
    )
})
