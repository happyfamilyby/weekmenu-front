import React from 'react'

import {
    Typography,
} from '@material-ui/core'

import {observer} from "mobx-react"

export const ShoppingList = observer(({ uiState }) => {

    return(
        <Typography variant="h2">Shopping List</Typography>
    )
})
