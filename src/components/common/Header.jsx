import React from 'react'

import {
    AppBar,
    Drawer,
    Toolbar,
    IconButton,
    Typography,
} from '@material-ui/core'
import { observer } from "mobx-react"
import { usePath } from 'hookrouter'

import MenuIcon from '@material-ui/icons/Menu'
import { Menu } from './Menu'

export const Header = observer(({ uiState }) => {

    const toggleDrawer = () => {
        uiState.toggleDrawer()
    }
    const currentPath = usePath().substring(1)

    return(
        <>
            <Drawer open={uiState.showDrawer} onClose={toggleDrawer}>
                <Menu uiState={uiState} />
            </Drawer>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu" onClick={toggleDrawer}>
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6">
                        { uiState.getPageTitle(currentPath) }
                    </Typography>
                </Toolbar>
            </AppBar>
        </>
    )
})
