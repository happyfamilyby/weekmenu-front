import React from 'react'
import { Restaurant, LocalGroceryStore, ListAlt, FavoriteBorder, Settings, HeadsetMic, PersonAdd } from '@material-ui/icons'
import { navigate } from 'hookrouter'

import {
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
} from '@material-ui/core'

import { UserCard } from './UserCard'
import { ui } from "../../store/ui"
import {observer} from "mobx-react"

const icons = {
    Restaurant: <Restaurant/>,
    LocalGroceryStore: <LocalGroceryStore/>,
    ListAlt: <ListAlt/>,
    FavoriteBorder: <FavoriteBorder/>,
    Settings: <Settings/>,
    HeadsetMic: <HeadsetMic/>,
    PersonAdd: <PersonAdd/>
}
export const Menu = observer(({ uiState }) => {

    const go = (menuItem) => {
        navigate(menuItem.route)
        uiState.setPageTitle(menuItem.title)
        uiState.toggleDrawer()
    }

    return(
        <List>
            <UserCard/>
            { ui.menu.map( ( menuItem, i ) =>
                    <ListItem button key={`menu-item-${i}`} onClick={() => go(menuItem)}>
                        <ListItemIcon>
                            { icons[menuItem.icon] }
                        </ListItemIcon>
                        <ListItemText primary={ menuItem.title } />
                    </ListItem>
                )
            }
        </List>
    )
})
