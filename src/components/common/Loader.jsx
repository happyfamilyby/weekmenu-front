import React from 'react'
import { observer } from "mobx-react"

import { CircularProgress, Backdrop } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    loader: {
        zIndex: theme.zIndex.drawer + 1,
    },
}));

export const Loader = observer(({ uiState }) => {

    const classes = useStyles();

    return(
        <Backdrop className={classes.loader} open={uiState.loading}>
            <CircularProgress />
        </Backdrop>
    )
})
