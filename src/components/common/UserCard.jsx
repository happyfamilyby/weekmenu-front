import React from 'react'

import {
    ListItem,
    ListItemText,
    ListItemAvatar,
    Avatar
} from '@material-ui/core'

import ImageIcon from '@material-ui/icons/Image'
import {makeStyles} from "@material-ui/core/styles"

export const UserCard = () => {

    const useStyles = makeStyles(theme => ({
        card: {
            backgroundColor: theme.palette.primary
        },
    }))

    const classes = useStyles()

    return(
        <ListItem className={classes.card}>
            <ListItemAvatar>
                <Avatar>
                    <ImageIcon />
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary="User Name" secondary="email@gmail.com" />
        </ListItem>
    )
}