import { createMuiTheme } from '@material-ui/core/styles'

export const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#ffa726',
            main: '#ff9800',
            dark: '#f57c00',
            contrastText: '#000',
        },
        secondary: {
            light: '#80d8ff',
            main: '#40c4ff',
            dark: '#00b0ff',
            contrastText: '#000',
        },
    },
})