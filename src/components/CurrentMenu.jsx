import React from 'react'

import {
    Typography,
} from '@material-ui/core'

import {observer} from "mobx-react"

export const CurrentMenu = observer(({ uiState }) => {

    return(
        <Typography variant="h2">Current Menu</Typography>
    )
})
