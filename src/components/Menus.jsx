import React, { useEffect } from 'react'

import {
    Typography,
} from '@material-ui/core'

import {observer} from "mobx-react"

export const Menus = observer(({ menusState }) => {

    useEffect(() => {
        menusState.getMenus()
    })

    return(
        <Typography variant="h2">Menus</Typography>
    )
})
