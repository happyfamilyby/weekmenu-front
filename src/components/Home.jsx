import React from 'react'

import {
    Typography,
} from '@material-ui/core'

import { ui } from "../store/ui"

export const Home = () => {

    ui.setPageTitle('Home')

    return(
        <Typography variant="h2">Home</Typography>
    )
}
