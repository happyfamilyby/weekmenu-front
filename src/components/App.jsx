import React from 'react'
import { useRoutes } from 'hookrouter'
import CssBaseline from '@material-ui/core/CssBaseline'

import { Header } from './common/Header'
import { Loader } from './common/Loader'

import { Home } from './Home'
import { CurrentMenu } from './CurrentMenu'
import { ShoppingList } from './ShoppingList'
import { Menus } from './Menus'
import { MyMenus } from './MyMenus'
import { Recipes } from './Recipes'
import { FavoriteRecipes } from './FavoriteRecipes'
import { Settings } from './Settings'
import { Support } from './Support'
import { Referral } from './Referral'

import { ThemeProvider } from '@material-ui/core/styles'
import { theme } from './apperance/theme'

import { ui } from "../store/ui"
import { menus } from "../store/menus"

export const App = () => {

    const routes = {
        "/": () => <Home/>,
        "/current-menu": () => <CurrentMenu/>,
        "/shopping-list": () => <ShoppingList/>,
        "/menus": () => <Menus menusState={menus} />,
        "/my-menus": () => <MyMenus/>,
        "/recipes": () => <Recipes/>,
        "/favorite-recipes": () => <FavoriteRecipes/>,
        "/settings": () => <Settings/>,
        "/support": () => <Support/>,
        "/referral": () => <Referral/>
    }

    const routeResult = useRoutes(routes)

    return(
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <Header uiState={ui} />
            { routeResult }
            <Loader uiState={ui} />
        </ThemeProvider>
    )
}
