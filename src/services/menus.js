import { apiClient } from '../apiClient'

export const apiGetMenus = async () => {
    apiClient.get('/menus')
        .then(function (response) {
            return response
        })
        .catch(function (error) {
            console.log(error)
        })
}