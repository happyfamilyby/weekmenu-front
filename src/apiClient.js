import axios from 'axios'

export const apiClient = axios.create({
    baseURL: 'http://178.124.206.44:8080/api',
    timeout: 2000
})