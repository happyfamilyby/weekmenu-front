## WeekMenu Technologies stack
* [React](https://ru.reactjs.org/)
* [MobX](https://mobx.js.org/README.html)
* [Material UI](https://material-ui.com/)
* [HookRouter](https://github.com/Paratron/hookrouter/blob/master/src-docs/pages/en/README.md)

## Доступные скрипты
Скрипты выполняются в директории проекта

#### `yarn start` или `npm start`

Этот скрипт запустит сервер для локальной разработки.
Он будет доступен по адресу [http://localhost:3000](http://localhost:3000)

Страница будет обновляться автоматически после каждого изменения.

#### `yarn build` или `npm run build`

Этот скрипт позволяет сбилдить приложение в JS и CSS фалйы, оптимизированные для использования в продакшене. Все файлы и ресурсы будут помещены в директорию `build`

Созданные файлы будут минифицированы и их имена будут содержать хеши.
После отработки этого скрипта, приложение готово к поставке.